# ---------------------------------------------------------------
# Build cbluezserver
# ---------------------------------------------------------------

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

$(BLUEZ_T)-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
$(BLUEZ_T)-get: .$(BLUEZ_T)-get

.$(BLUEZ_T)-get:
	@mkdir -p $(BLDDIR) 
ifeq ($(B5),)
	@if [ ! -f $(ARCDIR)/$(BLUEZ_PKG) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving bluez-tools source" $(EMSG); \
		$(MSG) "================================================================"; \
	    mkdir -p $(ARCDIR) ; \
		$(MSG3) "URL: $(BLUEZ_URL)" $(EMSG); \
		cd $(ARCDIR) && wget $(BLUEZ_URL); \
	else \
		$(MSG3) "BLUEZ source is cached" $(EMSG); \
	fi
else
	@if [ ! -d $(BLUEZ_SRCDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving bluez-tools source" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(BLDDIR) && git clone $(BLUEZ_URL) $(BLUEZ_VERSION); \
		cd $(BLDDIR)/$(BLUEZ_VERSION) && git checkout $(BLUEZ_COMMIT); \
	else \
		$(MSG3) "BLUEZ source is cached" $(EMSG); \
	fi
endif
	@touch .$(subst .,,$@)

$(BLUEZ_T)-get-patch: .$(BLUEZ_T)-get-patch

.$(BLUEZ_T)-get-patch: .$(BLUEZ_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
$(BLUEZ_T)-unpack: .$(BLUEZ_T)-unpack

.$(BLUEZ_T)-unpack: .$(BLUEZ_T)-get-patch
ifeq ($(B5),)
	@mkdir $(BLUEZ_SRCDIR)
	@tar --strip-components=1 -C $(BLUEZ_SRCDIR) -xvzf $(ARCDIR)/$(BLUEZ_PKG)
else
	@if [ ! -d $(BLUEZ_SRCDIR) ]; then \
		if [ -d $(BLUEZ_ARCDIR)/$(KSRC) ]; then \
			$(MSG3) "Copying archive to build dir" $(EMSG); \
			mkdir -p $(BLUEZ_SRCDIR); \
			rsync -a $(BLUEZ_ARCDIR)/$(KSRC)/ $(BLUEZ_SRCDIR)/; \
		else \
			$(MSG11) "source archive is missing:" $(EMSG); \
			$(MSG11) "$(BLUEZ_ARCDIR)/$(KSRC) not found " $(EMSG); \
			exit 1; \
		fi; \
	fi
endif
	@touch .$(subst .,,$@)

# Apply patches
$(BLUEZ_T)-patch: .$(BLUEZ_T)-patch

.$(BLUEZ_T)-patch: .$(BLUEZ_T)-unpack
	@touch .$(subst .,,$@)

$(BLUEZ_T)-init: .$(BLUEZ_T)-init

.$(BLUEZ_T)-init: 
	@make $(BLUEZ_T)-verify
	@make .$(BLUEZ_T)-patch
	@touch .$(subst .,,$@)

$(BLUEZ_T)-config: .$(BLUEZ_T)-config

.$(BLUEZ_T)-config: 
ifneq ($(B5),)
	cd $(BLUEZ_SRCDIR) && ./autogen.sh
endif
	cd $(BLUEZ_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
    	CC=$(XCC_PREFIX)-gcc \
    	CFLAGS=-I$(SD)/usr/include \
    	LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
		GLIB_LIBS=-lglib-2.0 \
    	./configure --host $(XCC_PREFIX) --disable-obex

    	# LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" 
# ./configure --host=arm-linux-gnueabi --prefix=
# PKG_CONFIG_PATH=/usr/arm-linux-gnueabi/lib/pkgconfig --disable-systemd --disable-udev
# --disable-cups --disable-obex --enable-library 

# Build the package
$(BLUEZ_T): .$(BLUEZ_T)

.$(BLUEZ_T): .$(BLUEZ_T)-init 
	@make --no-print-directory $(BLUEZ_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building BLUEZ" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(BLUEZ_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
    		CFLAGS=-I$(SD)/usr/include \
    		LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
    		DESTDIR=$(BLUEZ_BLDDIR) \
    		make 
	@cd $(BLUEZ_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
    		CFLAGS=-I$(SD)/usr/include \
    		LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
    		DESTDIR=$(BLUEZ_BLDDIR) \
    		make install
	@touch .$(subst .,,$@)

$(BLUEZ_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "BLUEZ Build Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(BLUEZ_SRCDIR)

# Package it as an opkg 
pkg $(BLUEZ_T)-pkg: opkg-verify .$(BLUEZ_T)
	@mkdir -p $(PKGDIR)/bluez-tools/CONTROL
	@cp -ar $(BLUEZ_BLDDIR)/* $(PKGDIR)/bluez-tools/
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/bluez-tools/CONTROL/control
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/bluez-tools/CONTROL/debian-binary
	@cd $(PKGDIR)/ && $(OPKG_DIR)/opkg-build -O bluez-tools

pkg-clean:
	@rm -rf $(PKGDIR)

# Clean out a cross compiler build but not the CT-NG package build.
$(BLUEZ_T)-clean:
	@rm -rf $(BLUEZ_SRCDIR) 
	@rm -f .$(BLUEZ_T) .$(BLUEZ_T)

# Clean out everything associated with BLUEZ
$(BLUEZ_T)-clobber: 
	@rm -rf $(PKGDIR) $(BLUEZ_SRCDIR) $(BLUEZ_BLDDIR) $(BLUEZ_ARCDIR)
	@rm -f .$(BLUEZ_T)-init .$(BLUEZ_T)-patch .$(BLUEZ_T)-unpack .$(BLUEZ_T)-get .$(BLUEZ_T)-get-patch
	@rm -f .$(BLUEZ_T) 

